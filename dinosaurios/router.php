<?php
    require_once("RouterClass.php");
    require_once("Controller/dinosaurio_controller.php");

    define("BASE_URL", 'http://'.$_SERVER["SERVER_NAME"].':'.$_SERVER["SERVER_PORT"].dirname($_SERVER["PHP_SELF"]).'/');
    
    $router = new Router();
    
    // "ruta", "VERBO", "Controlador", "metodo"

    // Mostrar todos
    $router->addRoute("dinosaurios", "GET", "DinosaurioController", "getAll");
    // Acá podría tener un formulario de inserción

    // Mostrar uno
    $router->addRoute("dinosaurio/:ID", "GET", "DinosaurioController", "get");
    // Acá podría tener un formulario de edición

    // Formulario para nuevo dinosaurio
    $router->addRoute("dinosaurio", "GET", "DinosaurioController", "formNew");

    // Acción formulario nuevo dinosaurio
    $router->addRoute("dinosaurio", "POST", "DinosaurioController", "new");

    // Acción de editar dinosaurio
    $router->addRoute("dinosaurio/:ID", "POST", "DinosaurioController", "edit");

    // Elimina un dinosaurio
    $router->addRoute("dinosaurio/:ID", "DELETE", "DinosaurioController", "delete");
    $router->addRoute("dinosaurio/:ID/delete", "GET", "DinosaurioController", "delete");
    
    // Por defecto mostrar todos
    $router->setDefaultRoute("DinosaurioController", "getAll");
    
    //run
    $router->route($_GET['action'], $_SERVER['REQUEST_METHOD']);

?>