<?php
    require_once('libs/Smarty.class.php');

    class SmartyView {

        public function __construct()
        {
            $this->smarty = new Smarty();
            $this->smarty->assign('BASE_URL', BASE_URL);
        }
    }

?>