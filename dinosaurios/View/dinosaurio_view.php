<?php
    require_once('View/smarty_view.php');

    class DinosaurioView extends SmartyView {

        public function showAll($dinosaurios)
        {
            $this->smarty->assign('dinosaurios', $dinosaurios);
            $this->smarty->display("templates/lista_dinosaurios.tpl");
        }

        public function show($dinosaurio)
        {
            $this->smarty->assign('dinosaurio', $dinosaurio);
            $this->smarty->display("templates/dinosaurio.tpl");
        }

        public function showFormNew()
        {
            include("templates/form_dinosaurio.php");
        }
    }


?>