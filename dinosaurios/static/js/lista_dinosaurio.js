function deleteClick(ev) {
    ev.preventDefault();
    let content = document.getElementById("content");
    fetch(this.href, { method: 'DELETE' })
        .then(res => res.text())
        .then(txt => {
            const parser = new DOMParser();
            const htmlNuevo = parser.parseFromString(txt, "text/html");
            const contentNuevo = htmlNuevo.getElementById("content");
            content.innerHTML = contentNuevo.innerHTML;
            setBorrar();
        });
    return false;
}

function setBorrar() {
    let botones = document.getElementsByClassName("delete");
    for (i = 0; i < botones.length; i++) {
        let boton = botones[i];
        boton.addEventListener("click", deleteClick);
    }
}

window.onload = function () {
    setBorrar();
};