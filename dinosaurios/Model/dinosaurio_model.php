<?php
    class DinosaurioModel{

        function __construct(){
            $this->dinosaurios = [
                ['id'=> 1, 'name'=>'Velocirraptor' , 'img' => 'velociraptor.jpg'],
                ['id'=> 2, 'name'=>'T-Rex'         , 'img' => 'trex.jpeg'],
                ['id'=> 3, 'name'=>'Bronquiosaurio']
            ];  
        }

        public function getAll()
        {
            return $this->dinosaurios;
        }

        public function get($id)
        {
            $dinosaurios = $this->dinosaurios;

            for($i = 0; $i<count($dinosaurios); $i++){
                $dinosaurio = $dinosaurios[$i];

                if($dinosaurio['id']==$id){
                    return $dinosaurio;
                }
            }
        }

        public function insert($name)
        {
            array_push($this->dinosaurios, ['id'=>4,'name'=>$name]);
        }

        public function edit($id, $name)
        {
            $dinosaurios = $this->dinosaurios;

            for($i = 0; $i<count($dinosaurios); $i++){
                $dinosaurio = $dinosaurios[$i];

                if($dinosaurio['id']==$id){
                    $this->dinosaurios[$i]['name'] = $name;
                }
            }
        }

        public function delete($id)
        {
            $dinosaurios = $this->dinosaurios;
            
            for($i = 0; $i<count($dinosaurios); $i++){
                $dinosaurio = $dinosaurios[$i];

                if($dinosaurio['id']==$id){
                    unset($this->dinosaurios[$i]);
                }
            }
        }
    }