<?php
    require_once("View/dinosaurio_view.php");
    require_once("Model/dinosaurio_model.php");

    class DinosaurioController
    {
        function __construct(){
            $this->view = new DinosaurioView();
            $this->model = new DinosaurioModel();
        }

        function getAll($params){
            $dinos = $this->model->getAll();
            $this->view->showAll($dinos);
        }

        function get($params){
            $id = $params[":ID"];
            $dino = $this->model->get($id);
            $this->view->show($dino);
        }

        public function formNew($params)
        {
            $this->view->showFormNew();
        }

        public function new($params)
        {
            if(isset($_POST['name'])){
                $name = $_POST['name'];
                $this->model->insert($name);
                $this->getAll([]);
            }
            else
            {
                // Si no hay $_POST vuelve al formulario
                $this->formNew([]);
            }
        }

        function edit($params){
            $id = $params[":ID"];
            if(isset($_POST['name'])){
                $name = $_POST['name'];
                $this->model->edit($id, $name);
            }
            $this->get($params);
        }

        function delete($params){
            $id = $params[":ID"];
            $this->model->delete($id);
            $this->getAll([]);
        }
    }

?>