<?php
    function producto($fila, $columna){
        return $fila * $columna;
    }

    function mostrarTabla($limite){
        $tabla = "<table>";
        for($fila = 1;$fila<$limite;$fila++){
            $tabla.="<tr>";
            for($columna=1;$columna<$limite;$columna++){
                $producto = producto($fila,$columna);
                $tabla.= "<td>" . $producto . "</td>";
            }
            $tabla.="</tr>";
        }
        $tabla.="</table>";
        return $tabla;
    }


    if(isset($_GET) && !empty($_GET) && isset($_GET["action"]) && $_GET["action"] != ""){
        $url = $_GET["action"];
    }
    $url = explode("/", $url); // "15/blah" => ["15", "blah"]
    $action = $url[0]; // "15"

    switch ($action) {
        case '5':
            $tabla = mostrarTabla($action);
            break;
        case '10':
            $tabla = mostrarTabla(10);
            break;
        case '15':
            $tabla = mostrarTabla(15);
            break;
        
        default:
            $tabla = mostrarTabla(99999);
            break;
    }

?>