<?php 
    define('BASE_URL', '//'.$_SERVER['SERVER_NAME'] . ':' . $_SERVER['SERVER_PORT'] . dirname($_SERVER['PHP_SELF']).'/');

    echo BASE_URL;
    echo "<br>";
    // BASE_URL = [http:]// localhost               :       80                 /web2/A/p2/listas                /     
    
    // url                              $_GET["action"] 
    // localhost/web2/A/p2/lista/       
    //                           ^^^
    //
    // localhost/web2/A/p2/lista/hola   hola       
    //                           ^^^
    //
    // localhost/web2/A/p2/lista/ho/la  ho/la       
    //                           ^^^
    //
    // localhost/web2/A/p2/listas/flores  flores       
    //                           ^^^
    // localhost/web2/A/p2/listas/arboles arboles       
    //                           ^^^

    // /...     /web2/A/p2/listas/.htaccess
    // localhost/web2/A/p2/listas/arboles/123      
    //                           ^
    // RewriteRule ^(.*)$ router.php?action=$1 [QSA,L] $1="arboles/123"
    // localhost/web2/A/p2/listas/router.php?action=arboles/123


    // Controlador

    function mostrarLista($lista){
        echo "<ul>";
        for($i = 0; $i < count($lista); $i++){
            echo "<li>" . $lista[$i] . "</li>";
        }
        echo "</ul>";
    }

    function mostrarListas($listas){
        for($j = 0; $j < count($listas); $j++){
            mostrarLista($listas[$j]);
        }
    }

    // Modelo

    $listaFlores = ["Margarita", "Tulipan", "Manzanilla", "Copete","Calendula"];
    $listaArboles = ["Nogal", "Sauce", "Roble", "Pino", "Cerezo", "Ciruelo"];
?>
<html>
<head>
    <base href="<?=BASE_URL?>">
</head>
<body>
<?php
    // Enrutador
    $url = "todas";

    if(isset($_GET) && !empty($_GET) && isset($_GET["action"]) && $_GET["action"] != ""){
        $url = $_GET["action"];
    }
    $url = explode("/", $url); // "flores/copete" => ["flores", "copete"]
    $action = $url[0]; // "flores"

    switch ($action) {
        case "todas":
            mostrarListas([$listaFlores, $listaArboles]);
            break;
        case 'flores':
            mostrarLista($listaFlores);
            break;
        case 'arboles':
            mostrarLista($listaArboles);
            break;
        
        default:
            echo "Error 404: Not found!";
            break;
    }
    
?>
<a href="arboles">Arboles</a>
<a href="flores">Flores</a>
<a href="">Todos</a>
</body>
</html>