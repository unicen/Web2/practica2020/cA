<!--

    Escriba un script php que muestre una página html completa generada desde el servidor con un encabezado de primer nivel (h1) con el famoso “Hola mundo!”.
    ¿Qué extensión debe tener la página? .php
    Lo que acabas de hacer: 
    ¿Es una página dinámica o una página estática? Estática
    ¿Cuál es la diferencia?
        La diferencia es que una página estatica no cambia.
    ¿Por qué es necesario tener un servidor web para realizar esto?
     * Necesita ejecutar el script php
     * Para que se pueda acceder desde la web
-->
<?php
    echo "<h1>Hola Mundo!</h1>";
    
    echo "<p> La hora actual es: ";
    echo date("H:i:s");
    echo "</p>";
?>