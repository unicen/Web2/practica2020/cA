<!--
    Construir un programa que calcule el índice de masa corporal de una persona 
        (IMC = peso [kg] / altura^2 [m]) e informe el estado en el que se encuentra esa persona en función del valor de IMC.

    Bajo peso<18,50
    Normal18,5 - 24,99
    Sobrepeso ≥25,00
    Obesidad ≥30,00


    TODO:
     * Formulario peso y alutra

     * funcion(peso, altura) -> IMC
     * function(IMC) -> estado (Bajo, Normal, etc)
     * Leer POST o GET y ejecutar
-->
<?php

    function IMC($peso, $altura){
        return $peso / ($altura * $altura);
    }

    function estado($IMC){
        $valor = "";
        if($IMC<18.5){
            $valor = "Bajo peso";
        }else if($IMC<25){
            $valor = "Normal";
        }else{
            $valor = "Sobrepeso";
            if($IMC>=30){
                $valor .= "y Obesidad";
            }
        }
        return $valor;
    }

    if(isset($_POST) && !empty($_POST)){
        if(isset($_POST["peso"]) && is_numeric($_POST["peso"])){
            $peso = $_POST["peso"];
        }
        if(isset($_POST["altura"]) && is_numeric($_POST["altura"])){
            $altura = $_POST["altura"] / 100;
        }
        if(isset($peso) && isset($altura)){
            $IMC = IMC($peso, $altura);
            $estado = estado($IMC);
        }
    }
?>
<html>
    <body>
        <form method="POST">
            <p>Peso: <input type="number" name="peso" /> kg</p>
            <p>Altura: <input type="number" name="altura" /> cm</p>
            <input type="submit" />
        </form>
        <?php if(isset($IMC)): ?>
            <p>Su indice de masa corporal es: <?=$IMC?>, y su estado es <?=$estado?></p>
        <?php endif; ?>
    </body>
</html>
