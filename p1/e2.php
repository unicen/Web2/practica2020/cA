<!--
    Escribir un programa que muestre una lista html generada desde el servidor a través de un arreglo. 
    Identifique las diferencias entre arreglos asociativos e indexados (ver docu oficial)

    Todos los arreglos en php son asociativos.
    Los arreglos indexados tienen como clave su posición.
-->
<?php
    function elementoListaHTML($valor){
        //$elementoHTML = "<li>" . $valor . "</li>";
        $elementoHTML = "<li>$valor</li>";
        return $elementoHTML;
    }
    
    function listaHTML($arreglo, $tipoLista){ // $tipoLista -> ul, ol
        $listaHTML = "<$tipoLista>";

        for($i = 0; $i < count($arreglo); $i++){
            $valor = $arreglo[$i]; // = 1
            $elementoHTML = elementoListaHTML($valor); // <li>1</li>
            $listaHTML .= $elementoHTML; 
        }

        $listaHTML .= "</$tipoLista>";
        return $listaHTML; // <ol> <li>1</li> <li>2</li> ... </ol>
    }

    //$miarreglo = array(1,2,3,4,5,6,8,9,0);
    $miarreglo = [1,2,3,4,5,6,8,9,0];

    $arregloAsociativo = array(2=>1, "a" => 1,2,3,4);

    print_r($arregloAsociativo);

    echo listaHTML($miarreglo, "ol");
    echo listaHTML($miarreglo, "ul");
?>