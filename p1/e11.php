<?php
    function elementoListaHTML($valor){
        //$elementoHTML = "<li>" . $valor . "</li>";
        $elementoHTML = "<li>$valor</li>";
        return $elementoHTML;
    }
    
    function listaHTML($arreglo, $tipoLista, $limite){ // $tipoLista -> ul, ol
        $listaHTML = "<$tipoLista>";

        for($i = 0; $i < count($arreglo) && $i < $limite; $i++){
            $valor = $arreglo[$i]; // = 1
            $elementoHTML = elementoListaHTML($valor); // <li>1</li>
            $listaHTML .= $elementoHTML; 
        }

        $listaHTML .= "</$tipoLista>";
        return $listaHTML; // <ol> <li>1</li> <li>2</li> ... </ol>
    }

    $miarreglo = [1,2,3,4,5,6,8,9,0,"a","b","c","d","e","f","g","h"];

    $limite = count($miarreglo);

    if(isset($_GET["limite"]) && is_numeric($_GET["limite"])){
        $limite = $_GET["limite"];
    }

    //echo listaHTML($miarreglo, "ol", $limite);

    $arregloResultado = [];
    for($i = 0; $i < $limite; $i++){
        $arregloResultado[$i] = $miarreglo[$i];
    }
    echo json_encode($arregloResultado);
?>