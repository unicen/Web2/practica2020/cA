<!--
    Escribir un programa que mediante un formulario html un usuario pueda ingresar su nombre, apellido y edad 
        para que sean enviados al servidor. 
        El programa, debe recibir los datos y mostrar la información por pantalla. 

    Envíe el formulario usando los métodos POST y GET. 
        ¿Cuál es la diferencia? 
        * Los parametros GET se pueden observar en la URL<2048
        * Los parametros POST están ocultos
        
        ¿En qué situaciones considera mejor utilizar uno u el otro?
        GET:
        * Cuando quiero poder compatrir la página con esos datos
        * Cuando quiero que queden el historial
        POST:
        * Cuando los datos son muchos
        * Información sensible (que no queremos que quede en historial/url)
        
        Investigue las diferencias entre los arreglos $_POST $_GET y $_REQUEST de PHP
    Genere validaciones de datos en el servidor. Ningún campo puede estar vacío.
     ¿Cuál es la diferencia entre realizar estas verificaciones del lado del cliente o del lado del servidor?
     Servidor:
        * El usuario no puede tocar codigo ni falsificar
     Cliente: 
        * Es mas facil para mostrar al usuario
        * No es necesrio refrescar la página
        * No consumo datos

    localhost/web2/A/p1/e3.php
        ? nombre= & apellido= & edad= & boton=2
        ? nombre=Nico & apellido=D & boton=1
-->
<?php
    if(!empty($_GET)){
        echo "<h1>Datos recibidos por GET</h1>";
        if(isset($_GET["nombre"])){
            $nombre = $_GET["nombre"];
            echo "<p>Su nombre es: $nombre</p>";
        }
        if(isset($_GET["apellido"])){
            $apellido = $_GET["apellido"];
            echo "<p>Su apellido es: $apellido</p>";
        }
        if(isset($_GET["edad"])){
            $edad = $_GET["edad"];
            echo "<p>Su edad es: $edad</p>";
        }
        if(isset($_GET["boton"])){
            $boton = $_GET["boton"];
            echo "<p>Apretó el boton: $boton</p>";
        }
    }


    if(!empty($_POST)){
        echo "<h1>Datos recibidos por POST</h1>";
        if(isset($_POST["nombre"])){
            $nombre = $_POST["nombre"];
            echo "<p>Su nombre es: $nombre</p>";
        }
        if(isset($_POST["apellido"])){
            $apellido = $_POST["apellido"];
            echo "<p>Su apellido es: $apellido</p>";
        }
        if(isset($_POST["edad"])){
            $edad = $_POST["edad"];
            echo "<p>Su edad es: $edad</p>";
        }
        if(isset($_POST["boton"])){
            $boton = $_POST["boton"];
            echo "<p>Apretó el boton: $boton</p>";
        }
    }

    if(!empty($_REQUEST)){
        echo "<h1>Datos recibidos por REQUEST</h1>";
        if(isset($_REQUEST["nombre"]) && $_REQUEST["nombre"] != ""){
            $nombre = $_REQUEST["nombre"];
            echo "<p>Su nombre es: $nombre</p>";
        }else{
            echo "<p>Su nombre no está seteado.</p>";
        }
        if(isset($_REQUEST["apellido"]) && $_REQUEST["apellido"] != ""){
            $apellido = $_REQUEST["apellido"];
            echo "<p>Su apellido es: $apellido</p>";
        }else{
            echo "<p>Su apellido no está seteado.</p>";
        }
        if(isset($_REQUEST["edad"]) && $_REQUEST["edad"] != ""){
            $edad = $_REQUEST["edad"];
            echo "<p>Su edad es: $edad</p>";
        }else{
            echo "<p>Su edad no está seteado.</p>";
        }
        if(isset($_REQUEST["boton"])){
            $boton = $_REQUEST["boton"];
            echo "<p>Apretó el boton: $boton</p>";
        }
    }

?>
<html>
    <body>
        <form method="POST" action="e3.php">
            <p>Nombre: <input type="text" name="nombre" /></p>
            <p>Apellido: <input type="text" name="apellido" /></p>
            <p>Edad: <input type="number" name="edad" /></p>
            <input type="submit" />
            <button type="submit" name="boton" value="3" >Otro Boton</button>
            <input type="submit" name="boton" value="1" />
            <input type="submit" name="boton" value="2" />
        </form>
    </body>
</html>
