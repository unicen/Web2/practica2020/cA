<?php

class Route {
    private $url;
    private $verb;
    private $controller;
    private $method;
    private $params;

    public function __construct($url, $verb, $controller, $method){
        $this->url = $url; // "libros"
        $this->verb = $verb; // "GET"
        $this->controller = $controller; // "LibrosController"
        $this->method = $method; // "getAll"
        $this->params = [];
    }
    public function match($url, $verb) {
        if($this->verb != $verb){
            return false;
        }
        $partsURL = explode("/", trim($url,'/')); //         "libro/123/EEE" -> ["libro", "123"] | "libros" -> ["libros"]
        $partsRoute = explode("/", trim($this->url,'/')); // "libro/:ID/:AA" -> ["libro", ":ID"] | "libros" -> ["libros"]
        if(count($partsRoute) != count($partsURL)){
            return false;
        }
        foreach ($partsRoute as $key => $part) {
            if($part[0] != ":"){
                if($part != $partsURL[$key])
                return false;
            } //es un parametro
            else
            $this->params[$part] = $partsURL[$key]; // " params[":ID"] -> "123" params[":AA"] -> "EEE"
        }
        return true;
    }
    public function run(){
        $controller = $this->controller;  // "LibrosController" 
        $method = $this->method;
        $params = $this->params;
        
        (new $controller())->$method($params);
        // (new "LibrosController"())->"getAll"($params)
    }
}

class Router {
    private $routeTable = []; 
    /* [ 
            Route("","GET","LibrosController", "getAll"),
            Route("libros","GET","LibrosController", "getAll") ,
            Route("libro/:ID","GET","LibrosController", "get")
        ]
    */
    private $defaultRoute; // Route("","","ErrorController", "show404")

    public function __construct() {
        $this->defaultRoute = null;
    }

    public function route($url, $verb) { // "libros", "GET"
        //$ruta->url //no compila!
        foreach ($this->routeTable as $route) {
            if($route->match($url, $verb)){
                //TODO: ejecutar el controller//ejecutar el controller
                // pasarle los parametros
                $route->run();
                return;
            }
        }
        //Si ninguna ruta coincide con el pedido y se configuró ruta por defecto.
        if ($this->defaultRoute != null)
            $this->defaultRoute->run();
    }
    
    public function addRoute ($url, $verb, $controller, $method) {
        $this->routeTable[] = new Route($url, $verb, $controller, $method);
    }

    public function setDefaultRoute($controller, $method) {
        $this->defaultRoute = new Route("", "", $controller, $method);
    }
}
