<?php
    require_once("RouterClass.php");
    require_once('Controller/user_controller.php');
    require_once('Controller/post_controller.php');
    require_once('Controller/comentario_controller.php');

    define("BASE_URL", 'http://'.$_SERVER["SERVER_NAME"].':'.$_SERVER["SERVER_PORT"].dirname($_SERVER["PHP_SELF"]).'/');
    
    define("THIS_URL", $_GET['action']);
    
    $router = new Router();

    // Ver el usuario
    $router->addRoute("user/:ID", "GET", "UsuarioController", "get");

    // Ver los posts del usuario
    $router->addRoute("user/:ID/posts", "GET", "PostController", "getByUser");

    // Ver un post con sus comentarios
    $router->addRoute("post/:ID/:nombre", "GET", "PostController", "get");

    // Crear un usuario
    $router->addRoute("user", "POST", "UserController", "insert");

    // Crear un post
    $router->addRoute("post", "POST", "PostController", "insert");

    // Crear un comentario
    $router->addRoute("comentario", "POST", "ComentarioController", "insert");
    
    // Editar un comentario
    $router->addRoute("comentario/:ID/edit", "GET", "ComentarioController", "formEdit");
    $router->addRoute("comentario/:ID", "POST", "ComentarioController", "edit");
    
    // Por defecto mostrar todos
    //$router->setDefaultRoute("DinosaurioController", "getAll");
    
    //run
    $router->route($_GET['action'], $_SERVER['REQUEST_METHOD']);

?>