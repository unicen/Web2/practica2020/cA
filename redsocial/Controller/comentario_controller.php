<?php
    require_once('Model/comentario_model.php');
    require_once('View/comentario_view.php');

    class ComentarioController{
    
        private $model;
        private $view;

        function __construct(){
            $this->model = new ComentarioModel();
            $this->view = new ComentarioView();
        }

        function get($params){
            $id = $params[':ID'];
            $comentario = $this->model->get($id);
            print_r($comentario);
        }

        function insert($params){
            if(
                isset($_POST['id_post']) 
                && $_POST['id_post'] != ""
                && isset($_POST['texto']) 
                && $_POST['texto'] != ""
            ){
                $id_post = $_POST['id_post'];
                $texto = $_POST['texto'];
                $result = $this->model->insert($id_post, $texto);
                
            }
        }

        function formEdit($params){
            $id = $params[':ID'];
            $comentario = $this->model->get($id);
            $return = "";
            if(isset($_GET['return'])){
                $return = $_GET['return'];
            }
            $this->view->showEdit($comentario, $return);
        }

        function edit($params){
            $id = $params[':ID'];
            if(isset($_POST['texto']) && $_POST['texto'] != ""){
                $texto = $_POST['texto'];
                $this->model->update($id, $texto);
            }
            if(isset($_GET['return'])){
                header("Location:" . BASE_URL . $_GET['return']);
            }
        }

    }
    