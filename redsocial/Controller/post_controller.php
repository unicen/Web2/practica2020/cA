<?php
    require_once('Model/usuario_model.php');
    require_once('Model/post_model.php');
    require_once('Model/comentario_model.php');
    require_once('View/post_view.php');

    class PostController{
    
        private $model;
        private $userModel;
        private $commentsModel;
        private $view;

        function __construct(){
            $this->model = new PostModel();
            $this->userModel = new UsuarioModel();
            $this->commentsModel = new ComentarioModel();
            $this->view = new PostView();
        }

        function get($params){
            $id = $params[':ID'];
            $post = $this->model->get($id);
            $id_user = $post->id_usuario;
            $user = $this->userModel->get($id_user);
            $comments = $this->commentsModel->getAllByPost($id);
            $this->view->show($user, $post, $comments);
        }

        function insert($params){
            if(
                isset($_POST['id_usuario']) 
                && $_POST['id_usuario'] != ""
                && isset($_POST['texto']) 
                && $_POST['texto'] != ""
            ){
                $id_usuario = $_POST['id_usuario'];
                $texto = $_POST['texto'];
                $result = $this->model->insert($id_usuario, $texto);
            }
        }

    }
    