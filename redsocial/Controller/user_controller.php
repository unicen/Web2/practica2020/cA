<?php
    require_once('Model/usuario_model.php');

    class UsuarioController
    {
        private $model;
        private $view;

        function __construct(){
            $this->model = new UsuarioModel();
        }

        function get($params){
            $id = $params[':ID'];
            $user = $this->model->get($id);
            print_r($user);
        }
    }
    