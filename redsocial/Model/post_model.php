<?php
    require_once('Model/model.php');

    class PostModel extends Model
    {
        function getAll(){
            // Construllo una sentencia
            $sentencia = $this->db->prepare("SELECT id,id_usuario,texto FROM post");

            // ejecuto la sentencia
            $sentencia->execute();

            // Traer a una variable todos los registros encontrados
            $posts = $sentencia->fetchAll(PDO::FETCH_OBJ);

            return $posts;
        }

        function getAllByUser($id_usuario){
            // Construllo una sentencia
            $sentencia = $this->db->prepare("SELECT id,id_usuario,texto FROM post WHERE id_usuario = ?");

            // ejecuto la sentencia
            $sentencia->execute([$id_usuario]);

            // Traer a una variable todos los registros encontrados
            $posts = $sentencia->fetchAll(PDO::FETCH_OBJ);

            return $posts;
        }

        function get($id){
            // Construllo una sentencia
            $sentencia = $this->db->prepare("SELECT id,id_usuario,texto FROM post WHERE id = ?");

            // ejecuto la sentencia
            $sentencia->execute([$id]);

            $post = $sentencia->fetch(PDO::FETCH_OBJ);

            return $post;
        }


        function insert($id_usuario, $texto){
            // Construllo una sentencia
            $sentencia = $this->db->prepare("INSERT INTO post(id_usuario,texto) VALUES (?,?)");

            // ejecuto la sentencia
            return $sentencia->execute([$id_usuario, $texto]);            
        }

        public function update($id, $texto)
        {
            // Construllo una sentencia
            $sentencia = $this->db->prepare("UPDATE post SET texto = ? WHERE id = ?;");

            // ejecuto la sentencia
            return $sentencia->execute([$texto,$id]);      
        }

        function delete($id){
            // Construllo una sentencia
            $sentencia = $this->db->prepare("DELETE FROM post WHERE id = ?");

            // ejecuto la sentencia
            return $sentencia->execute([$id]);
        }
    }
    