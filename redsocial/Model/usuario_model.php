<?php
    require_once('Model/model.php');

    class UsuarioModel extends Model
    {
        function getAll(){
            // Construllo una sentencia
            $sentencia = $this->db->prepare("SELECT id,nombre FROM usuario");

            // ejecuto la sentencia
            $sentencia->execute();

            // Traer a una variable todos los registros encontrados
            $usuarios = $sentencia->fetchAll(PDO::FETCH_OBJ);

            return $usuarios;
        }

        function get($id){
            // Construllo una sentencia
            $sentencia = $this->db->prepare("SELECT id,nombre,contraseña FROM usuario WHERE id = ?");

            // ejecuto la sentencia
            $sentencia->execute([$id]);

            $usuario = $sentencia->fetch(PDO::FETCH_OBJ);

            return $usuario;
        }


        function insert($nombre, $contraseña, $email){
            // Construllo una sentencia
            $sentencia = $this->db->prepare("INSERT INTO usuario(nombre,contraseña,email) VALUES (?,?,?)");

            // ejecuto la sentencia
            return $sentencia->execute([$nombre, $contraseña, $email]);            
        }

        public function update($id, $nombre, $contraseña, $email)
        {
            // Construllo una sentencia
            $sentencia = $this->db->prepare("UPDATE usuario SET nombre = ?,contraseña = ?, email=? WHERE id = ?;");

            // ejecuto la sentencia
            return $sentencia->execute([$nombre, $contraseña, $email,$id]);      
        }

        function delete($id){
            // Construllo una sentencia
            $sentencia = $this->db->prepare("DELETE FROM usuario WHERE id = ?");

            // ejecuto la sentencia
            return $sentencia->execute([$id]);
        }
    }
    