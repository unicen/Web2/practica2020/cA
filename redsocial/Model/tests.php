<?php 
    // TEST DESTRUCTIVO!!! hacer backup antes de probar.
    function test_usuario(){
        $model = new UsuarioModel();
        $model->insert('Nico','123','nico@web.com');
        $model->insert('Otro','123','otro@web.com');
        $model->insert('Tres','123','tres@web.com');
        
        $all = $model->getAll();

        echo "All:<br>";
        print_r($all);
        echo "<br><br>";

        echo "update:<br>";
        print_r($model->update($all[1]->id, "NuevoNombre", "NuevaPass", "nuevomail@web.com"));
        echo "<br><br>";

        echo "get:<br>";
        print_r($model->get($all[1]->id));
        echo "<br><br>";

        echo "Delete:<br>";
        foreach ($all as $key => $user) {
            print_r($model->delete($user->id));
        }
    }