<?php
    require_once('Model/model.php');

    class ComentarioModel extends Model
    {
        function getAll(){
            // Construllo una sentencia
            $sentencia = $this->db->prepare("SELECT id,id_post,texto FROM comentario");

            // ejecuto la sentencia
            $sentencia->execute();

            // Traer a una variable todos los registros encontrados
            $comentarios = $sentencia->fetchAll(PDO::FETCH_OBJ);

            return $comentarios;
        }

        function getAllByPost($id_post){
            // Construllo una sentencia
            $sentencia = $this->db->prepare("SELECT id,id_post,texto FROM comentario WHERE id_post = ?");

            // ejecuto la sentencia
            $sentencia->execute([$id_post]);

            // Traer a una variable todos los registros encontrados
            $comentarios = $sentencia->fetchAll(PDO::FETCH_OBJ);

            return $comentarios;
        }

        function get($id){
            // Construllo una sentencia
            $sentencia = $this->db->prepare("SELECT id,id_post,texto FROM comentario WHERE id = ?");

            // ejecuto la sentencia
            $sentencia->execute([$id]);

            $comentario = $sentencia->fetch(PDO::FETCH_OBJ);

            return $comentario;
        }


        function insert($id_post, $texto){
            // Construllo una sentencia
            $sentencia = $this->db->prepare("INSERT INTO comentario(id_post,texto) VALUES (?,?)");

            // ejecuto la sentencia
            return $sentencia->execute([$id_post, $texto]);            
        }

        public function update($id, $texto)
        {
            // Construllo una sentencia
            $sentencia = $this->db->prepare("UPDATE comentario SET texto = ? WHERE id = ?;");

            // ejecuto la sentencia
            return $sentencia->execute([$texto,$id]);      
        }

        function delete($id){
            // Construllo una sentencia
            $sentencia = $this->db->prepare("DELETE FROM comentario WHERE id = ?");

            // ejecuto la sentencia
            return $sentencia->execute([$id]);
        }
    }
    