<?php
    require_once('View/smarty_view.php');

    class ComentarioView extends SmartyView
    {
        function show($comment)
        {
            $this->smarty->assign('comentario', $comment);
            $this->smarty->display("templates/comentario_show.tpl");
        }

        function showEdit($comment, $return)
        {
            $this->smarty->assign('comentario', $comment);
            $this->smarty->assign('return', $return);
            $this->smarty->display("templates/comentario_edit.tpl");
        }
    }
    