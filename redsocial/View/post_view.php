<?php
    require_once('View/smarty_view.php');

    class PostView extends SmartyView
    {
        function show($user, $post, $comments, $mensaje = null)
        {
            $this->smarty->assign('usuario', $user);
            $this->smarty->assign('post', $post);
            $this->smarty->assign('comentarios', $comments);
            $this->smarty->display("templates/post_show.tpl");
        }
    }
    